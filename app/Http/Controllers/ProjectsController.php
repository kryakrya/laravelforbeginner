<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectsController extends Controller
{
    public function index()
    {
    	// $projects=\App\Project::all();
    	$projects = Project::all();
    	// $projects = auth()->user()->projects;

    	// return $projects;
    	// return view('projects.index', ['projects' => $projects]);
    	return view('projects.index', compact('projects'));
    }

    public function create()
    {
    	return view('projects.create');
    }

    // public function show($id)
    // {
    //     $project = Project::findOrFail($id);

    //     // return $project;
    //     return view('projects.show', compact('project'));
    // }

    public function show(Project $project)
    {

        // return $project;
        return view('projects.show', compact('project'));
    }

    // public function edit($id) //example.com/projects/1/edit/
    // {
    //     // return $id;
    //     // $project = Project::find($id);
    //     $project = Project::findOrFail($id);
    //     return view('projects.edit', compact('project'));
    // }

    public function edit(Project $project) //example.com/projects/1/edit/
    {

        return view('projects.edit', compact('project'));
    }


    // public function update()
    // {
    //     // dd('hello!');
    //     dd(request()->all());
    // }
    // public function update($id)
    // {
    //     // $project = Project::find($id);
    //     $project = Project::findOrFail($id);
    //     $project->title = request('title');
    //     $project->description = request('description');


    //     $project->save();
    //     return redirect('/projects');
    // }

    public function update(Project $project)
    {
        $project->update(request(['title', 'description'])); 
        // $project->title = request('title');
        // $project->description = request('description');


        // $project->save();
        return redirect('/projects');
    }

    // public function destroy($id)
    // {
    //     // dd($id.' deleted');
    //     // Project::find($id)->delete();
    //     Project::findOrFail($id)->delete();

    //     return redirect('/projects');
    // }

    public function destroy(Project $project)
    {
        $project->delete();

        return redirect('/projects');
    }


    public function store()
    {
        // Project::create([
        //     'title'       => request('title'),
        //     'description' => request('description')
        // ]);
        // request()->validate([
        //     'title' => ['required', 'min:3' ],
        //     'description' => ['required', 'min:3']
        // ]);

        $validated = request()->validate([
            'title' => ['required', 'min:3' ],
            'description' => ['required', 'min:3']
        ]);
        Project::create($validated);
        // Project::create(request()->all());
    	// $project = new Project();
    	// $project->title = request('title');
    	// $project->description = request('description');

    	// $project->save();

    	return redirect('/projects');
    	// return request()->all();
    }
}
