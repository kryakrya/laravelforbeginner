@extends('layout')


@section('content')
	
	<h1 class="title">Edit Project</h1>

	<!-- <form method="PATCH" action="/project/{{ $project->id }}"> -->
		<form method="POST" action="/projects/{{ $project->id }}">
		{{ method_field('PATCH') }}
		{{ csrf_field() }}
		
		<div class="field">
			
			<label class="label" for="title">Title</label>

			<div class="control">
				<input type="text" class="input" name="title" placeholder="Title" value="{{ $project->title }}">
			</div>
		</div>

		<div class="field">
			
			<label class="label" for="description">Title</label>

			<div class="control">
				<textarea class="textarea" name="description"> {{ $project->description }} </textarea>
			</div>
		</div>

		<div class="field">

			<div class="control">
				<button type="submit" class="button is-link">Update Project</button>
			</div>
		</div>


	</form>
	<br>
	<form method="POST" action="/projects/{{ $project->id }}">
		<!-- {{ method_field('DELETE') }}
		{{ csrf_field() }} -->
		@method('DELETE')
		@csrf
		<div class="field">

			<div class="control">
				<button type="submit" class="button is-danger">Delete Project</button>
			</div>
		</div>

		<div class="notification is-danger container">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>

	</form>

@endsection