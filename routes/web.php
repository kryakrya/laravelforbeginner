<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// exmaple.com/show-post.php

Route::get('/home', 'HomeController@index')->name('home');

/* 
*	GET /projects (index) //display
*	GET /projects/create //create page
*	GET /projects/1  //show
*	POST /projects (store) //create
*	GET /projects/1/edit //edit
*	PUT
*	PATCH /projects/1/update //update
*   DELETE /projects/1/destroy
*/

Route::resource('projects', 'ProjectsController');
// Route::get('/projects', 'ProjectsController@index');
// Route::get('/projects/create', 'ProjectsController@create');
// Route::get('/projects/{projects}', 'ProjectsController@show');
// Route::post('/projects', 'ProjectsController@store');
// Route::get('/projects/{project}/edit', 'ProjectsController@edit');
// Route::patch('/projects/{project}', 'ProjectsController@update');
// Route::delete('/projects/{project}', 'ProjectsController@destroy');